#!/bin/sh

VER=$1

mkdir rex-io-webui-plugin-xen-$VER
cp -R lib rex-io-webui-plugin-xen-$VER

tar czf rex-io-webui-plugin-xen-$VER.tar.gz rex-io-webui-plugin-xen-$VER

rm -rf rex-io-webui-plugin-xen-$VER
