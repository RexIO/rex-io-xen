#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::IO::WebUI::Xen;

use Mojo::Base 'Mojolicious::Controller';
use Data::Dumper;
use File::Basename 'dirname';

sub new_vm {
   my ($self) = @_;

   my $ref = $self->req->json;

   my $ret = $self->rexio->call(POST => "1.0", "xen", 
      server => $self->param("id"),
      vm     => $self->param("name"),
      ref    => $ref);

   return $self->render(json => $ret);
}

sub vm_action {
   my ($self) = @_;

   my $ret = $self->rexio->call(POST => "1.0", "xen", server => $self->param("id"), vm => $self->param("name"), $self->param("vm_action") => undef);

   return $self->render(json => $ret);
}

sub switch_vm_mode {
   my ($self) = @_;

   my $ret = $self->rexio->call(POST => "1.0", "xen", server => $self->param("id"), vm => $self->param("name"), mode => $self->param("mode"));

   return $self->render(json => $ret);
}

sub __register__ {
   my ($self, $routes) = @_;
   my $r      = $routes->{route};
   my $r_auth = $routes->{route_auth};

   $r_auth->post("/xen/server/:id/vm/:name")->to("xen#new_vm");
   $r_auth->post("/xen/server/:id/vm/:name/:vm_action")->to("xen#vm_action");
   $r_auth->post("/xen/server/:id/vm/:name/mode/:mode")->to("xen#switch_vm_mode");
}

sub __init__ {
   my ($class, $app) = @_;

   # add plugin template path
   push(@{ $app->renderer->paths }, dirname(__FILE__) . "/templates");
   push(@{ $app->static->paths }, dirname(__FILE__) . "/public");
}

1;
