(function() {

   $(document).ready(function() {


   });

})();

var xen_table_init_done = 0;

var xen_oTable;

function prepare_xen_vm_table() {

   if(! $("#table_xen_vms_wrapper")[0]) {

      xen_oTable = $("#table_xen_vms").dataTable({
         "bJQueryUI": true,
         "bPaginate": false,
         "sScrollY": 300,
         "sPaginationType": "full_numbers"
      });

      prepare_data_tables();

      $("#table_xen_vms tbody tr").click( function( e ) {
         if ( $(this).hasClass('row_selected') ) {
            $(this).removeClass('row_selected');
         }
         else {
            xen_oTable.$('tr.row_selected').removeClass('row_selected');
            $(this).addClass('row_selected');
         }
      });

      return xen_oTable;

   }


}

function list_xen_vms(srv_id) {

   $("#add_xen_vm").dialog({
      autoOpen: false,
      height: 420,
      width: 400,
      modal: true,
      buttons: {
         "Add": function() {
            $.log("adding");
            
            add_xen_vm(srv_id, {
               name: $("#vm_name").val(),
               vcpus: $("#vm_vcpus").val(),
               memory: $("#vm_memory").val(),
               ip: $("#vm_ip").val(),
               netmask: $("#vm_netmask").val(),
               os: $("#vm_os").val(),
               disk: $("#vm_disk").val(),
               callback: function() {
                  // reload server view
                  xen_view_reload(srv_id);
               }
            });

            $(this).dialog("close");
         },
         Cancel: function() {
            $(this).dialog("close");
         }
      },
      close: function() {
         $("INPUT").val("").removeClass("ui-state-error");
      }
   });

   $("#lnk_xen_new_vm").click(function() {
      $("#add_xen_vm").dialog("open");
   });

   $(".lnk_xen_mode").click(function(event) {
      event.preventDefault();
      var type = $(this).attr("type");
      var selected_row = fnGetSelected(xen_oTable);
      var vm_id = $(selected_row).attr("vm_id");
      var srv_id = $(selected_row).attr("srv_id");

      xen_switch_vm_mode({
         "server": srv_id,
         "vm": vm_id,
         "type": type
      });
   });

   $(".lnk_xen_vm_action").click(function() {
      var selected_row = fnGetSelected(xen_oTable);
      var vm_id = $(selected_row).attr("vm_id");
      var srv_id = $(selected_row).attr("srv_id");
      var vm_action = $(this).attr("vm_action");

      $.log(vm_action + ": " + srv_id + "/" + vm_id);

      xen_vm_action(srv_id, vm_id, vm_action);
   });

   $("#lnk_xen_reload").click(function() {
      xen_view_reload(srv_id);
   });

   $(".vm-link").click(function(event) {
      event.preventDefault();
      load_server($(this).attr("hw_id"));
   });

}

function add_xen_vm(srv_id, ref) {

   $.ajax({
      "url": "/xen/server/" + srv_id + "/vm/" + ref.name,
      "type": "POST",
      "data": JSON.stringify({
         "vcpus": ref.vcpus,
         "memory": ref.memory,
         "os": ref.os,
         "ip": ref.ip,
         "netmask": ref.netmask,
         "disk": ref.disk,
      }),
      "contentType": "application/json",
      "dataType": "json"
   }).done(function(data) {
      try {
         if(data.ok != true) {
            throw "Error adding new xen vm";
         }
         else {
            if(typeof ref.callback != "undefined") {
               ref.callback();
            }
         }
      } catch(err) {
         $.log(err);
      }
   });
}

function xen_vm_action(srv_id, vm_id, action) {
   $.log(action + ": vm: " + vm_id + " on " + srv_id);

   if(
      action == "delete" ||
      action == "stop"
   ) {
      xen_vm_action_dialog(srv_id, vm_id, action);
   }
   else {

      $.ajax({
         "url": "/xen/server/" + srv_id + "/vm/" + vm_id + "/" + action,
         "type": "POST",
         "dataType": "json"
      }).done(function(data) {
         try {
            if(data.ok != true) {
               $.log("Error " + action + " VM");
            }
            else {
               $.pnotify({
                  text: "Command '" + action + "' sent to Hypervisor",
                  type: "info"
               });
 
               xen_view_reload(srv_id);
            }
         } catch(err) {
            $.log(err);
         }
      });

   }
}

function xen_vm_action_dialog(srv_id, vm_id, action) {

   dialog_confirm({
      id: "xen_action_dialog",
      title: "Really " + action + " " + vm_id + " on " + server_name,
      text: "This VM will " + action + " " + vm_id + " on server " + server_name + ". Are you sure?",
      button: action,
      ok: function() {
         $.ajax({
            "url": "/xen/server/" + srv_id + "/vm/" + vm_id + "/" + action,
            "type": "POST",
         }).done(function(data) {

            $.pnotify({
               text: "Server " + action + "ed",
               type: "info"
            });

            xen_view_reload(srv_id);
         });
      },
      cancel: function() {}
   });

}

function xen_view_reload(srv_id) {
   load_server(srv_id, function() {
      activate_tab($("#li-tab-xen"));
   });
}

function xen_switch_vm_mode(ref) {

   dialog_confirm({
      id: "xen_switch_vm_mode_dialog",
      title: "Really switch to " + ref.type + " mode?",
      text: "This will switch the virtualization mode to: " + (ref.type == "hvm" ? "full" : "para") + " virtualization<br /><br />You have to stop and start the VM after this manually.",
      height: 200,
      button: "Switch",
      ok: function() {
         $.ajax({
            "url": "/xen/server/" + ref.server + "/vm/" + ref.vm + "/mode/" + ref.type,
            "type": "POST",
         }).done(function(data) {

            $.pnotify({
               text: "Virualization switched to " + ref.type,
               type: "info"
            });

            xen_view_reload(ref.server);
         });
      },
      cancel: function() {}
   });


}

