#!/bin/sh

VER=$1

mkdir rex-io-server-plugin-xen-$VER
cp -R {db,lib} rex-io-server-plugin-xen-$VER

tar czf rex-io-server-plugin-xen-$VER.tar.gz rex-io-server-plugin-xen-$VER

rm -rf rex-io-server-plugin-xen-$VER
