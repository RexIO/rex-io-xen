Summary: Rex.IO - Middleware - Xen Plugin
Name: rex-io-server-plugin-xen
Version: 0.2.16
Release: 1
License: Apache 2.0
Group: Utilities/System
Source: http://rex.io/downloads/rex-io-server-plugin-xen-0.2.16.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
AutoReqProv: no

BuildRequires: rexio-perl >= 5.18.0
#Requires: libssh2 >= 1.2.8 - is included in perl-Net-SSH2 deps
Requires: rexio-perl >= 5.18.0

%description
Xen Plugin for Rex.IO Server

%prep
%setup -n %{name}-%{version}


%install
%{__rm} -rf %{buildroot}
%{__mkdir} -p %{buildroot}/srv/rexio/rex-io-xen/server
%{__cp} -R {db,lib} %{buildroot}/srv/rexio/rex-io-xen/server/

### Clean up buildroot
find %{buildroot} -name .packlist -exec %{__rm} {} \;

%post

/bin/chown -R rexio. /srv/rexio
grep rex-io-xen /etc/rex/io/env-server.sh >/dev/null 2>&1
if [ "$?" != "0" ]; then
   echo 'PERL5LIB=$PERL5LIB:/srv/rexio/rex-io-xen/server/lib' >>/etc/rex/io/env-server.sh
fi

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root, 0755)
/srv/rexio/rex-io-xen/server

%changelog

* Thu Jul 25 2013 Jan Gehring <jan.gehring at, gmail.com> 0.2.16-1
- initial packaged

