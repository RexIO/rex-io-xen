--
-- Table structure for table `opennebula`
--

DROP TABLE IF EXISTS `xen_vms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xen_vm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hardware_id` int(11) DEFAULT NULL,
  `vm_name` varchar(255),
  `hypervisor_hardware_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


