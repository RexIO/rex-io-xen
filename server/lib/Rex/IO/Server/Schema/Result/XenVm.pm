#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::IO::Server::Schema::Result::XenVm;

use strict;
use warnings;

use base qw(DBIx::Class::Core);

__PACKAGE__->load_components(qw/InflateColumn::DateTime/);
__PACKAGE__->table("xen_vm");
__PACKAGE__->add_columns(qw/id hardware_id vm_name hypervisor_hardware_id/);

__PACKAGE__->set_primary_key("id");

__PACKAGE__->belongs_to("hardware", "Rex::IO::Server::Schema::Result::Hardware", "hardware_id");
__PACKAGE__->belongs_to("hypervisor", "Rex::IO::Server::Schema::Result::Hardware", "hypervisor_hardware_id");

Rex::IO::Server::Schema::Result::Hardware->has_many("xen_vms" => __PACKAGE__, "hypervisor_hardware_id");
Rex::IO::Server::Schema::Result::Hardware->has_one("vm_guest" => __PACKAGE__, "hardware_id");

1;
