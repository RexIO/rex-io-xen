#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::IO::Server::Xen;
   
use Mojo::Base 'Mojolicious::Controller';
use Mojo::JSON;
use Mojo::UserAgent;
use Rex::IO::Server::Helper::IP;

use Data::Dumper;

sub list_vms_on_host {
   my ($self) = @_;

   my $hw = $self->db->resultset("Hardware")->find($self->param("id"));

   my $ip = $hw->primary_ip;

   my $ref = $self->_ua->get("http://$ip:4000/vms")->res->json;
   my @ret;

   my $data = $ref->{data};

   for my $vm (@{ $data }) {
      my $name = $vm->{name};
      $self->app->log->debug("searching vm: $name");
      eval {
         my $hw = $self->db->resultset("XenVm")->search({ vm_name => $name })->first->hardware;
         $vm->{hardware} = $hw->to_hashRef;
         push @ret, $vm;
      };
   }

   $ref->{data} = [ @ret ];

   return $self->render(json => $ref);
}

sub has_xen {
   my ($self) = @_;

   my $hw = $self->db->resultset("Hardware")->find($self->param("id"));
   my $xen = $hw->xen;

   if($xen) {
      return $self->render(json => { ok => Mojo::JSON->true });
   }

   return $self->render(json => { ok => Mojo::JSON->false }, status => 404);
}

sub create_vm {
   my ($self) = @_;

   my $hypervisor = $self->db->resultset("Hardware")->find($self->param("id"));
   my $ip = $hypervisor->primary_ip;

   my $json = $self->req->json;
   my $name = $self->param("name");


   my $ref = $self->_ua->post("http://$ip:4000/vm/$name", json => $json)->res->json;

   if($ref->{ok} == Mojo::JSON->true) {
      my $mac = $ref->{mac};

      my $hw = $self->db->resultset("Hardware")->create({
         name => $name,
         uuid => $mac,
         state_id => 1,
         os_template_id => $json->{os},
      });

      $hw->update;

      my $nw_a = $self->db->resultset("NetworkAdapter")->create({
         hardware_id => $hw->id,
         proto       => "static",
         boot        => 1,
         mac         => $mac,
         ip          => ip_to_int($json->{ip}),
         netmask     => ip_to_int($json->{netmask}),
         wanted_ip   => ip_to_int($json->{ip}),
         wanted_netmask => ip_to_int($json->{netmask}),
      });

      $nw_a->update;

      # update dhcp server
      $self->_ua->post($self->config->{dhcp}->{server} . "/" . $nw_a->mac, json => {
         name => $name,
         ip   => int_to_ip($nw_a->wanted_ip),
      });

      # add vm to table
      my $vm_a = $self->db->resultset("XenVm")->create({
         hardware_id => $hw->id,
         vm_name     => $name,
         hypervisor_hardware_id => $hypervisor->id,
      });

      $vm_a->update;
   }

   $self->render(json => $ref);
}

sub vm_action {
   my ($self) = @_;

   my $action = $self->param("vm_action");
   my $name = $self->param("name");
   my $hw = $self->db->resultset("Hardware")->find($self->param("id"));
   my $ip = $hw->primary_ip;

   my $json = $self->req->json;

   my $ref = $self->_ua->post("http://$ip:4000/vm/$name/$action")->res->json;

   if($action eq "delete") {
      # delete also the server
      my $hw_i = $self->db->resultset("Hardware")->search({ name => $self->param("name") })->first;

      eval {
         $self->_ua->delete($self->config->{dhcp}->{server} . "/" . $hw_i->name);
      } or do {
         $self->app->log->error("error deregistering " . $hw_i->name . " on dhcp server: $@");
      };

      eval {
         if(my $hw = $hw_i) {
            $hw->purge;
         }
      };
   }

   $self->render(json => $ref);
}

sub list_devices {
   my ($self) = @_;

   my $hw = $self->db->resultset("Hardware")->find($self->param("id"));
   my $ip = $hw->primary_ip;

   my $json = $self->req->json;

   my $ref = $self->_ua->get("http://$ip:4000/devices")->res->json;

   return $self->render(json => $ref);
}

sub switch_vm_mode {
   my ($self) = @_;

   my $vm_name = $self->param("name");
   my $vm_mode = $self->param("vm_mode");
   my $hw = $self->db->resultset("Hardware")->find($self->param("id"));
   my $ip = $hw->primary_ip;

   my $json = $self->req->json;

   my $ref = $self->_ua->post("http://$ip:4000/vm/$vm_name/mode/$vm_mode")->res->json;

   return $self->render(json => $ref);
}

sub __register__ {
   my ($self, $app) = @_;
   my $r = $app->routes;

   $r->route("/1.0/xen/server/:id/vm")->via("LIST")->to("xen#list_vms_on_host");
   $r->route("/1.0/xen/server/:id")->via("INFO")->to("xen#has_xen");
   $r->route("/1.0/xen/server/:id/vm/:name")->via("POST")->to("xen#create_vm");
   $r->route("/1.0/xen/server/:id/vm/:name/:vm_action")->via("POST")->to("xen#vm_action");
   $r->route("/1.0/xen/server/:id/devices")->via("LIST")->to("xen#list_devices");
   $r->route("/1.0/xen/server/:id/vm/:name/mode/:vm_mode")->via("POST")->to("xen#switch_vm_mode");
}

sub __delete_hardware__ {
   my ($self, $mojo, $hw) = @_;
   $mojo->app->log->debug("(xen) delete hardware... " . $hw->name);
   my $xen_vm = $hw->vm_guest;
   if(! $xen_vm) {
      return;
   }

   my $hypervisor = $xen_vm->hypervisor;
   if(! $hypervisor) {
      return;
   }

   $mojo->app->log->debug("(xen) (hypervisor)... " . $hypervisor->name);

   my $ip = $hypervisor->primary_ip;
   if(! $ip) {
      return;
   }

   my $name = $hw->name;
   my $ref = $self->_ua->post("http://$ip:4000/vm/$name/delete")->res->json;

   eval {
      $self->_ua->delete($self->config->{dhcp}->{server} . "/" . $hw->name);
   } or do {
      $self->app->log->error("error deregistering " . $hw->name . " on dhcp server: $@");
   };

}


sub __inventor__ {
   my ($class, $hw, $db, $ref) = @_;

   if($ref->{xen}) {
      my $listen_ip = $ref->{xen}->{XEN_LISTEN_IP};
      my $listen_port = $ref->{xen}->{XEN_LISTEN_PORT};

      if($listen_ip eq "*") {
         $listen_ip = $hw->primary_ip;
      }

      my $rpc = "http://$listen_ip:$listen_port";

      my $xen = $db->resultset("Xen")->create({
         rpc => $rpc,
         hardware_id => $hw->id,
      });
   }
}

sub _ua {
   my ($self) = @_;
   return Mojo::UserAgent->new;
}


1;
