#!/bin/sh

VER=$1

mkdir rex-io-service-os-daemon-plugin-xen-$VER
cp -R bin rex-io-service-os-daemon-plugin-xen-$VER

tar czf rex-io-service-os-daemon-plugin-xen-$VER.tar.gz rex-io-service-os-daemon-plugin-xen-$VER

rm -rf rex-io-service-os-daemon-plugin-xen-$VER
