Summary: Rex.IO - Service Daemon - Xen Plugin
Name: rex-io-service-os-daemon-plugin-xen
Version: 0.2.2
Release: 1
License: Apache 2.0
Group: Utilities/System
Source: http://rex.io/downloads/rex-io-service-os-daemon-plugin-xen-0.2.2.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
AutoReqProv: no

BuildRequires: rexio-perl >= 5.18.0
#Requires: libssh2 >= 1.2.8 - is included in perl-Net-SSH2 deps
Requires: rexio-perl >= 5.18.0

%description
Xen Service OS Daemon Plugin for Rex.IO

%prep
%setup -n %{name}-%{version}


%install
%{__rm} -rf %{buildroot}
%{__mkdir} -p %{buildroot}/etc/rex/io/inventory
%{__cp} -R bin/inventory/* %{buildroot}/etc/rex/io/inventory/

### Clean up buildroot
find %{buildroot} -name .packlist -exec %{__rm} {} \;

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root, 0755)
/etc/rex/io/inventory

%changelog

* Thu Jul 25 2013 Jan Gehring <jan.gehring at, gmail.com> 0.2.2-1
- initial packaged

